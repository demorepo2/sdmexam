const express = require('express')
const cors = require('cors')

const app = express()
app.use(cors('*'))
app.use(express.json())

app.get('/', (request, response) => {
    response.send('Welcome inside get request')
}) 
app.post('/', (request, response) => {
    response.send('Welcome inside post request')
}) 
app.put('/', (request, response) => {
    response.send('Welcome inside put request')
}) 
app.delete('/', (request, response) => {
    response.send('Welcome inside delete request')
}) 

app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
})